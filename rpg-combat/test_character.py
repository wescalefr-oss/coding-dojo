import pytest

from character import Character, MAX_LIFE, Position, CharacterType, Table


def test_check_character_when_created():
    character = Character()
    assert character.health == MAX_LIFE
    assert character.level == 1
    assert character.is_alive is True


def test_character_can_deal_damage():
    defender = Character()
    attacker = Character()
    attacker.damage(target=defender, count=100)
    assert defender.health == MAX_LIFE - 100


def test_character_die_if_health_lower_than_0():
    defender = Character()
    attacker = Character()
    attacker.damage(target=defender, count=1100)
    assert defender.health == 0
    assert defender.is_alive is False


def test_character_cannot_damage_itself():
    character = Character()
    character.damage(target=character, count=100)
    assert character.health == MAX_LIFE


def test_character_with_fulllife_cannot_be_healed():
    healer = Character()

    healer.heal(count=100)

    assert healer.health == MAX_LIFE


def test_dead_character_cannot_be_healed():
    # GIVEN
    dead_character = Character()
    dead_character.receive_damage(count=MAX_LIFE)

    # WHEN
    dead_character.heal(count=100)
    # THEN
    assert dead_character.health == 0
    assert dead_character.is_alive is False


def test_character_can_heal_itself():
    character = Character()
    character.health = 500
    character.heal(count=100)
    assert character.health == 600


def test_damage_are_reduced_by_50_percent_when_level_receiver_is_5_or_more_above_the_attacker():
    receiver = Character()
    attacker = Character()
    attacker.level = 5
    receiver.level = 11

    attacker.damage(receiver, 10)

    assert receiver.health == MAX_LIFE - (10 / 2)


def test_damage_are_increased_by_50_percent_when_level_attacker_is_5_or_more_above_the_receiver():
    receiver = Character()
    attacker = Character()
    attacker.level = 11
    receiver.level = 5

    attacker.damage(receiver, 10)

    assert receiver.health == MAX_LIFE - (10 * 1.5)


def test_melee_attacker_can_reach_opponent_is_his_range_of_2():
    melee_attacker = Character(Position(0, 3))
    receiver = Character(Position(0, 2))

    melee_attacker.damage(receiver, 10)

    assert receiver.health == MAX_LIFE - 10


def test_melee_attacker_cannot_reach_opponent_when_not_in_his_range_of_2():
    melee_attacker = Character(Position(0, 3))
    receiver = Character(Position(0, 0))

    melee_attacker.damage(receiver, 10)

    assert receiver.health == MAX_LIFE


def test_ranger_attacker_can_reach_opponent_when_not_in_his_range_of_2():
    ranger_attacker = Character(Position(0, 3), CharacterType.RANGER)
    receiver = Character(Position(0, 0))

    ranger_attacker.damage(receiver, 10)

    assert receiver.health == MAX_LIFE - 10


def test_new_character_has_no_factions():
    character = Character()

    assert character.factions == []


def test_new_character_can_join_faction_wescale_paris():
    character = Character()
    character.join_faction("Wescale-Paris")
    assert character.factions == ["Wescale-Paris"]


def test_new_character_can_join_faction_wescale_paris_nantes():
    character = Character()
    character.join_faction("Wescale-Paris")
    character.join_faction("Wescale-Nantes")
    assert character.factions == ["Wescale-Paris", "Wescale-Nantes"]


def test_new_character_can_leave_faction_wescale_paris_remote():
    character = Character()
    character.join_faction("Wescale-Paris")
    character.join_faction("Wescale-Nantes")
    character.join_faction("Wescale-Remote")
    character.leave_faction("Wescale-Remote")
    character.leave_faction("Wescale-Paris")
    assert character.factions == ["Wescale-Nantes"]


def test_new_character_can_join_factions_wescale_paris_remote():
    character = Character()
    character.join_factions(["Wescale-Paris", "Wescale-Nantes"])
    assert character.factions == ["Wescale-Paris", "Wescale-Nantes"]


def test_new_character_can_leave_factions_wescale_paris_nantes():
    character = Character()
    character.join_factions(["Wescale-Paris", "Wescale-Nantes", "Wescale-Remote"])
    character.leave_factions(["Wescale-Paris", "Wescale-Nantes"])
    assert character.factions == ["Wescale-Remote"]


def test_new_character_cannot_leave_factions_when_not_in_faction():
    character = Character()
    character.join_factions(["Wescale-Remote"])
    character.leave_factions(["Wescale-Paris", "Wescale-Nantes"])
    assert character.factions == ["Wescale-Remote"]


def test_two_same_faction_character_cannot_hit_each_other():
    mathieu = Character()
    mathieu.join_faction("Wescale-Paris")
    gerome = Character()
    gerome.join_faction("Wescale-Paris")
    gerome.damage(mathieu, 900)
    assert mathieu.health == MAX_LIFE


def test_two_same_faction_character_can_heal_each_other():
    mathieu = Character(health=MAX_LIFE / 2)
    mathieu.join_faction("Wescale-Paris")
    gerome = Character()
    gerome.join_faction("Wescale-Paris")

    gerome.heal(MAX_LIFE / 2, mathieu)

    assert mathieu.health == MAX_LIFE


def test_two_different_faction_character_cannot_heal_each_other():
    mathieu = Character(health=MAX_LIFE / 2)
    mathieu.join_faction("Wescale-Nantes")
    gerome = Character()
    gerome.join_faction("Wescale-Paris")

    gerome.heal(MAX_LIFE / 2, mathieu)

    assert mathieu.health == MAX_LIFE / 2


def test_character_can_damage_table():
    mathieu = Character()
    table = Table()

    mathieu.damage(target=table, count=30)

    assert table.health == MAX_LIFE - 30


def test_table_cannot_deal_damage_to_character():
    mathieu = Character()
    table = Table()

    try:
        table.damage(target=mathieu, count=30)
        pytest.fail("test is failed")
    except AttributeError as err:
        pass

    assert mathieu.health == MAX_LIFE


def test_table_cannot_be_healed_by_a_character():
    mathieu = Character()
    table = Table(health=MAX_LIFE / 2)

    try:
        mathieu.heal(count=30, target=table)
        pytest.fail("test is failed")
    except AttributeError:
        pass

    assert table.health == MAX_LIFE / 2


def test_table_cannot_be_member_of_a_faction():
    table = Table()

    try:
        table.join_faction(faction="Ici c'est Paris")
        pytest.fail("test is failed")
    except AttributeError:
        pass


def test_table_is_destructed_if_health_is_zero():
    table = Table()

    table.receive_damage(count=MAX_LIFE)

    assert table.is_destructed is True
