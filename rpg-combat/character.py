from __future__ import annotations
from enum import Enum
import math

MAX_LIFE = 1000
DELTA_LEVEL_IMPACT = 5
FACTOR_DECREASE_IMPACT = 0.5
FACTOR_INCREASE_IMPACT = 1.5
MELEE_RANGE = 2
RANGER_RANGE = 20


class CharacterType(Enum):
    MELEE = MELEE_RANGE
    RANGER = RANGER_RANGE


class Position:

    def __init__(self, x=0, y=0) -> None:
        self.x = x
        self.y = y

    def is_reachable(self, other_position, range_):
        return math.dist([other_position.x, other_position.y], [self.x, self.y]) < range_


class Things:

    def __init__(self, position=Position(0, 0), health=MAX_LIFE) -> None:
        self.health = health
        self.level = 1
        self.position = position

    def receive_damage(self, count):
        self.health -= count
        if self.health <= 0:
            self.health = 0

    @property
    def is_destructed(self):
        return self.health == 0


class Character(Things):

    def __init__(self, position=Position(0, 0), character_type=CharacterType.MELEE, health=MAX_LIFE) -> None:
        super().__init__(position, health)
        self.character_type = character_type
        self.factions = []

    @property
    def range(self):
        return self.character_type.value

    @property
    def is_alive(self):
        return self.health > 0

    def join_faction(self, faction: str) -> None:
        self.factions.append(faction)

    def join_factions(self, factions: list) -> None:
        self.factions += factions

    def leave_factions(self, factions: list) -> None:
        self.factions = [i for i in self.factions if i not in factions]

    def leave_faction(self, faction: str) -> None:
        self.factions.remove(faction)

    def is_allied_with(self, other: Character):
        if self == other:
            return True

        list_ = [i for i in self.factions if i in other.factions]
        return list_ != []

    def damage(self, target: Things, count: int):
        if self != target and self.position.is_reachable(target.position, self.range):
            if not self.is_allied_with(other=target):
                computed_damage = self._compute_damage(count, target)
                target.receive_damage(computed_damage)

    def _compute_damage(self, count, defender):
        _damage = count

        if (defender.level - self.level) > DELTA_LEVEL_IMPACT:
            _damage = int(count * FACTOR_DECREASE_IMPACT)
        elif (self.level - defender.level) > DELTA_LEVEL_IMPACT:
            _damage = int(count * FACTOR_INCREASE_IMPACT)

        return _damage

    def heal(self, count: int, target: Character = None):
        if target is None:
            target = self

        if self.can_heal(target):
            target.health = min(target.health + count, MAX_LIFE)

    def can_heal(self, target) -> bool:
        return target.is_alive and target.is_allied_with(self)


class Table(Things):
    pass
